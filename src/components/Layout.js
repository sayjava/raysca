import React from "react"
import Helmet from "react-helmet"
import { StaticQuery, graphql } from "gatsby"

import "./layout.module.css"

export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`

export default ({ children }) => (
  <StaticQuery
    query={query}
    render={data => {
      return (
        <>
          <Helmet
            title={data.site.siteMetadata.title}
            meta={[
              { name: "description", content: "Raysca Dev Website" },
              {
                name: "keywords",
                content: "tech, javascript, golang, developer",
              },
            ]}
          >
            <html lang="en" />
          </Helmet>
          <header>
            {/* <nav>
              <ul>
                <li>
                  <a href="/">Home</a>
                </li>
              </ul>
            </nav> */}
          </header>
          {children}
          <footer>Copyright Raysca creative Ltd</footer>
        </>
      )
    }}
  />
)
