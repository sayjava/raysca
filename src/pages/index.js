import React from "react"
import Layout from "../components/Layout"

import style from "./home.module.css"

export const query = graphql`
  query {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
            stack
            summary
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
  }
`

const skillData = [
  {
    title: "Web",
    icon: "html5",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation",
  },
  {
    title: "Mobile",
    icon: "node-js",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation",
  },
  {
    title: "Cloud",
    icon: "docker",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation",
  },
  {
    title: "K8",
    icon: "dev",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation",
  },
]

const Hero = () => (
  <section className={style.hero}>
    <div className={style.inner}>
      <h1 className={style.title}>Raysca Dev</h1>
      <p className={style.tagline}>
        A serious developer interested in machine learning and other stuff
      </p>
    </div>
  </section>
)

const Skill = ({ title, description, icon }) => (
  <div className={style.skill}>
    <i className={`fab fa-${icon}`} />
    <h4>{title}</h4>
    <p>{description}</p>
  </div>
)

const Skills = () => (
  <section className={style.skills}>
    <h1 className={style.skillTitle}>Skills</h1>
    <div className={style.skillset}>
      {skillData.map(skill => (
        <Skill key={skill.title} {...skill} />
      ))}
    </div>
  </section>
)

const Project = ({ project: { fields, frontmatter } }) => (
  <div className={style.project}>
    <h3>
      <a href={fields.slug}>{frontmatter.title}</a>
    </h3>
    <h5>Stack: {frontmatter.stack}</h5>
    <p>{frontmatter.summary}</p>
  </div>
)

export default ({
  data: {
    allMarkdownRemark: { edges },
  },
}) => (
  <Layout>
    <>
      <Hero />
      <Skills />
      <section>
        <h1>Projects</h1>
        <div className={style.projects}>
          {edges.map(({ node }) => (
            <Project key={node.id} project={node} />
          ))}
        </div>
      </section>
    </>
  </Layout>
)
